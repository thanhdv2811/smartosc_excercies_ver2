<?php
$installer = $this;
 
$installer->startSetup();
 
$installer->run("
 
-- DROP TABLE IF EXISTS {$this->getTable('sm_slider')};
CREATE TABLE {$this->getTable('sm_slider')} (
  `slider_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '1',
  `show_title` smallint(6) NOT NULL default '1',  
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('sm_slider_items')};
CREATE TABLE {$this->getTable('sm_slider_items')} (
  `slider_item_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '1',
  `slider_id` text NOT NULL default '',
  `alt` varchar(255) NOT NULL default '',
  `image` text NOT NULL default '',
  `url` text NOT NULL default '',
  `text` text NOT NULL default '',
  `sort_order` int(11) NOT NULL default '0',
  PRIMARY KEY (`slider_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
    ");
 
$installer->endSetup();