<?php

class SM_Slider_Block_Widget extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getSlider()
    {
        $sliderId = $this->getSliderId();
        $sliderItemCollection = Mage::getModel('slider/slideritem')
            ->getCollection()
            ->addFieldToFilter('status',array('eq' => 0))
            ->addFieldToFilter('slider_id',array('finset' => $sliderId))
            ->setOrder('sort_order','asc')
            ->load();

        return $sliderItemCollection;
    }

    public function getTitleSlider()
    {
        $sliderModel = Mage::getModel('slider/slider')->load($this->getSliderId());
        if ($sliderModel->getData('show_title') == 0) {
            return $sliderModel->getData('title');
        } else {
            return false;
        }
    }

}