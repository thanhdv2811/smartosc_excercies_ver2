<?php

class SM_Slider_Block_Slider extends Mage_Core_Block_Template
{

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCurrentSliderCategory()
    {
        // camelCase
        $currentCategory = Mage::registry('current_category');
        $categoryModel = Mage::getModel('catalog/category')->load($currentCategory->getId());
        $sliderId = $categoryModel->getCategorySlide();

        if ($sliderId == 0) {
            return false;
        }

        $sliderItemCollection = Mage::getModel('slider/slideritem')
            ->getCollection()
            ->addFieldToFilter('status',array('eq' => 0))
            ->addFieldToFilter('slider_id',array('finset' => $sliderId))
            ->setOrder('sort_order','asc')
            ->load();

        return $sliderItemCollection;
    }

    public function getTitleSlider()
    {
        $currentCategory = Mage::registry('current_category');
        $categoryModel = Mage::getModel('catalog/category')->load($currentCategory->getId());
        $sliderId = $categoryModel->getCategorySlide();
        $sliderModel = Mage::getModel('slider/slider')->load($sliderId);

        if ($sliderModel->getData('show_title') == 0) {
            return $sliderModel->getData('title');
        } else {
            return false;
        }
    }

    public function getHomeSlider()
    {
        $homeSliderId = Mage::getStoreConfig('slider/general/home_page_slider');

        if($homeSliderId == 0) {
            return false;
        }

        $sliderItemCollection = Mage::getModel('slider/slideritem')
            ->getCollection()
            ->addFieldToFilter('status',array('eq' => 0))
            ->addFieldToFilter('slider_id',array('finset' => $homeSliderId))
            ->setOrder('sort_order','asc')
            ->load();

        return $sliderItemCollection;
    }

    public function getTitleHomeSlider()
    {
        $loadConfig    = Mage::getStoreConfig('slider/general/home_page_slider');
        $sliderModel   = Mage::getModel('slider/slider')->load($loadConfig);

        if($sliderModel->getData('show_title') == 0) {
            return $sliderModel->getData('title');
        } else {
            return false;
        }
    }

}
