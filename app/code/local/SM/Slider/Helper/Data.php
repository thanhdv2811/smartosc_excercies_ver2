<?php

class SM_Slider_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function deleteImageFile($image) {
        if (!$image) {
            return;
        }
        $bannerImagePath = Mage::getBaseDir('media') . DS . $image;
        if (!file_exists($bannerImagePath)) {
            return;
        }

        try {
            unlink($bannerImagePath);
        } catch (Exception $e) {
            $logger = Mage::getModel('core/logger');
            $logger>log($e->getMessage());
        }
    }

}