<?php

class SM_Slider_Model_Option_Slider extends Varien_Object
{
    protected $_options = array();

    public function getOptionArray()
    {
        $sliderCollection = Mage::getModel('slider/slider')->getCollection();
        foreach ($sliderCollection as $slider) {
            $this->_options[] = array(
                'label' => $slider->getData('title'),
                'value' => $slider->getData('slider_id')
            );
        }

        return $this->_options;
    }

}
