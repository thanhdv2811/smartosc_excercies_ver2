<?php

class SM_FeatureProduct_Block_Categoryfeature extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getFeatureProduct()
    {
        $currentCategory = Mage::registry('current_category');
        $categoryModel = Mage::getModel('catalog/category')->load($currentCategory->getId());
        $products = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->addCategoryFilter($categoryModel)
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sm_feature', array('eq' => '1'));
        Mage::getSingleton('catalog/product_status')
            ->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')
            ->addVisibleInCatalogFilterToCollection($products);
        $products->addAttributeToSort('created_at', 'desc')
            ->setPageSize(20);

        return $products;
    }
}