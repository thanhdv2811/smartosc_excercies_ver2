<?php

class SM_FeatureProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function checkLayout()
    {
        $rootLayout = Mage::app()->getLayout()->getBlock('root')->getTemplate();
        if (strpos($rootLayout,'3columns') !== false) {
            return 3;
        } elseif(strpos($rootLayout,'2columns') !== false) {
            return 4;
        } else {
            return 5;
        }
    }
}
