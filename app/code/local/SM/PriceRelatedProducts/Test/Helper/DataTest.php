<?php
/**
 * Created by PhpStorm.
 * User: thanhdv
 * Date: 8/5/15
 * Time: 5:15 PM
 */
class SM_PriceRelatedProducts_Test_Helper_Data extends EcomDev_PHPUnit_Test_Case
{
    protected function _mockProduct($numberProduct){
        $arrayProductMock = array();
        for($i = 0; $i <= $numberProduct; $i++) {
            $productMock = $this->getModelMock('catalog/product',array('getName'));
            $productMock->expects($this->atLeastOnce())
                ->method('getName')
                ->willReturn('Product Name Number '.$i);
            $arrayProductMock[] = $productMock;
        }

        return $arrayProductMock;
    }

    /**
     * @covers SM_PriceRelatedProducts_Helper_Data::getAllProductName()
     * @param $productQuantity
     * @test
     * @dataProvider dataProvider
     */
    public function getAllProductNameTest($productQuantity)
    {
        $collectionData = $this->_mockProduct($productQuantity);
        $collection = $this->getMock('ArrayIterator', array('load'), array($collectionData));
        $collection->expects($this->any())
            ->method('load')
            ->willReturnSelf();

        $productMock = $this->getModelMock('catalog/product',array('getCollection'));
        $productMock->expects($this->once())
            ->method('getCollection')
            ->willReturn($collection);

        $this->replaceByMock('model', 'catalog/product', $productMock);

        $helperResult = Mage::helper('pricerelatedproducts')->getAllProductName();
        $expectArray = array();
        for($i = 0; $i <= $productQuantity; $i++) {
            $expectArray[] = 'Product Name Number '.$i;
        }

        $this->assertEquals($expectArray,$helperResult);
    }
}