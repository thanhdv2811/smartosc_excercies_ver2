<?php

class SM_PriceRelatedProducts_Test_Block_Catalog_Product_List_RelatedTest extends EcomDev_PHPUnit_Test_Case
{

    /**
     * setUp run before every test run
     */
    public function setUp()
    {
        $sessionCheckoutMock = $this->getModelMockBuilder('checkout/session')
            ->disableOriginalConstructor()
            ->setMethods(array('getQuoteId'))
            ->getMock();

        $sessionCheckoutMock->expects($this->any())
            ->method('getQuoteId')
            ->willReturn('2000');

        $this->replaceByMock('singleton', 'checkout/session', $sessionCheckoutMock);

        $sessionCartMock = $this->getResourceModelMock('checkout/cart', array('addExcludeProductFilter'));

        $sessionCartMock->expects($this->any())
            ->method('addExcludeProductFilter')
            ->willReturnSelf();

        $this->replaceByMock('resource_singleton', 'checkout/cart', $sessionCartMock);

        parent::setUp();
    }

    /**
     *
     * Create a mock for catalog/product
     * @param $productId
     * @param $productPrice
     *
     */
    protected function _mockProductModel($productId, $productPrice)
    {
        $productCollection = Mage::getResourceModel('catalog/product_collection');

        $productMock = $this->getModelMock('catalog/product', array(
            'getRelatedProductCollection',
            'getCollection',
            'getId',
            'getFinalPrice'
        ));

        $productMock->expects($this->any())
            ->method('getRelatedProductCollection')
            ->willReturn($productCollection);

        $productMock->expects($this->any())
            ->method('getId')
            ->willReturn($productId);

        $productMock->expects($this->any())
            ->method('getFinalPrice')
            ->willReturn($productPrice);

        $productMock->expects($this->any())
            ->method('getCollection')
            ->willReturn($productCollection);

        $this->replaceByMock('model', 'catalog/product', $productMock);

        Mage::unregister('product');

        Mage::register('product', $productMock);
    }

    /**
     * Create a mock for catalog/product_collection
     */
    protected function _getProductResourceCollectionMock()
    {
        $productCollectionMock = $this->getResourceModelMockBuilder('catalog/product_collection')
            ->disableOriginalConstructor()
            ->setMethods(
                array(
                    'addAttributeToSelect',
                    'setPositionOrder',
                    'addStoreFilter',
                    'load',
                    'getSize',
                    'addAttributeToFilter',
                    'getSelect',
                )
            )
            ->getMock();

        $productCollectionMock->expects($this->any())
            ->method('addAttributeToSelect')
            ->willReturnSelf();

        $productCollectionMock->expects($this->any())
            ->method('setPositionOrder')
            ->willReturnSelf();

        $productCollectionMock->expects($this->any())
            ->method('addStoreFilter')
            ->willReturnSelf();

        $productCollectionMock->expects($this->any())
            ->method('load')
            ->willReturnSelf();

        return $productCollectionMock;
    }

    /**
     * Create a mock for session catalog/product_visibility
     * @param $collection
     */
    protected function _mockCatalogProductVisibility($collection)
    {
        $catalogProductVisibilityMock = $this->getModelMock('catalog/product_visibility', array('addVisibleInCatalogFilterToCollection'));

        $catalogProductVisibilityMock->expects($this->any())
            ->method('addVisibleInCatalogFilterToCollection')
            ->willReturn($collection);

        $this->replaceByMock('singleton', 'catalog/product_visibility', $catalogProductVisibilityMock);
    }

    /**
     * create a mock for block related "pricerelatedproducts/catalog_product_list_related"
     */
    protected function _getRelatedBlockMock()
    {
        $blockRelated = $this->getBlockMock('pricerelatedproducts/catalog_product_list_related',
            array(
                '_addProductAttributesAndPrices',
                '_getPriceRelatedProduct'
            )
        );

        $blockRelated->expects($this->any())
            ->method('_addProductAttributesAndPrices')
            ->willReturnSelf();

        return $blockRelated;
    }

    /**
     *
     * Test function _prepareData() when product has related product
     * @test
     * @dataProvider dataProvider
     * @param $productId
     * @param $productPrice
     * @param $relatedProductNumber
     *
     */
    public function testHasRelatedProduct($productId, $productPrice, $relatedProductNumber)
    {
        $productCollectionMock = $this->_getProductResourceCollectionMock();

        $productCollectionMock->expects($this->any())
            ->method('getSize')
            ->willReturn($relatedProductNumber);

        $this->replaceByMock('resource_model', 'catalog/product_collection', $productCollectionMock);

        $this->_mockProductModel($productId, $productPrice);

        $this->_mockCatalogProductVisibility($productCollectionMock);

        $blockRelated = $this->_getRelatedBlockMock();

        $this->replaceByMock('block', 'pricerelatedproducts/catalog_product_list_related', $blockRelated);

        $blockRelated->expects($this->never())->method('_getPriceRelatedProduct');

        EcomDev_Utils_Reflection::invokeRestrictedMethod($blockRelated, '_prepareData');
    }

    /**
     *
     * Test function _prepareData() when product does not have related product
     * @test
     * @dataProvider dataProvider
     * @param $productId
     * @param $productPrice
     * @param $relatedProductNumber
     *
     */
    public function testNotHasRelatedProduct($productId, $productPrice, $relatedProductNumber)
    {
        $productCollectionMock = $this->_getProductResourceCollectionMock();

        $productCollectionMock->expects($this->any())
            ->method('getSize')
            ->willReturn($relatedProductNumber);

        $this->replaceByMock('resource_model', 'catalog/product_collection', $productCollectionMock);

        $this->_mockProductModel($productId, $productPrice);

        $this->_mockCatalogProductVisibility($productCollectionMock);

        $blockRelated = $this->_getRelatedBlockMock();

        $blockRelated->expects($this->once())->method('_getPriceRelatedProduct')->willReturnSelf();

        $this->replaceByMock('block', 'pricerelatedproducts/catalog_product_list_related', $blockRelated);

        EcomDev_Utils_Reflection::invokeRestrictedMethod($blockRelated, '_prepareData');
    }

    /**
     *
     * Test function _getPriceRelatedProduct()
     * @test
     * @dataProvider dataProvider
     * @param $productId
     * @param $productPrice
     *
     */
    public function testGetPriceRelatedProduct($productId, $productPrice)
    {
        $varienDbSelectMock = $this->getMockBuilder('Varien_Db_Select')
            ->setMethods(array('columns', 'order'))
            ->disableOriginalConstructor()
            ->getMock();

        $varienDbSelectMock->expects($this->once())
            ->method('columns')
            ->with(array('price_distance' => new Zend_Db_Expr('ABS(price_index.final_price - ' . $productPrice . ')')))
            ->willReturnSelf();

        $varienDbSelectMock->expects($this->once())
            ->method('order')
            ->with($this->equalTo('price_distance'))
            ->willReturnSelf();

        $productCollectionMock = $this->_getProductResourceCollectionMock();

        $productCollectionMock->expects($this->any())
            ->method('addAttributeToFilter')
            ->with('entity_id', array('neq' => $productId))
            ->willReturnSelf();

        $productCollectionMock->expects($this->any())
            ->method('getSelect')
            ->willReturn($varienDbSelectMock);

        $this->replaceByMock('resource_model', 'catalog/product_collection', $productCollectionMock);

        $this->_mockProductModel($productId, $productPrice);

        $this->_mockCatalogProductVisibility($productCollectionMock);

        $blockRelated = $this->getBlockMock('pricerelatedproducts/catalog_product_list_related',
            array(
                '_addProductAttributesAndPrices'
            )
        );

        $blockRelated->expects($this->any())
            ->method('_addProductAttributesAndPrices')
            ->willReturnSelf();

        $this->replaceByMock('block', 'pricerelatedproducts/catalog_product_list_related', $blockRelated);

        EcomDev_Utils_Reflection::invokeRestrictedMethod($blockRelated, '_getPriceRelatedProduct');
    }
}
