<?php

class SM_PriceRelatedProducts_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getAllProductName()
    {
        $productCollection = Mage::getModel('catalog/product')->getCollection();
        $productNameArray = array();
        foreach($productCollection as $product) {
            $productNameArray[] = $product->getName();
        }

        return $productNameArray;
    }
}
