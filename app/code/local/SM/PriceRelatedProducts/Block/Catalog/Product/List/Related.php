<?php

class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    const PRODUCT_LIMIT = 5;
    protected function _prepareData()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getRelatedProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter()
        ;

        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }

        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        if (!$this->_itemCollection->getSize()) {
            return $this->_getPriceRelatedProduct();
        } else {
            return $this;
        }
    }

    protected function _getPriceRelatedProduct()
    {
        $product = Mage::registry('product');
        /* @var $product Mage_Catalog_Model_Product */

        $this->_itemCollection = $product->getCollection()
            ->addAttributeToSelect('required_options')
            ->addAttributeToFilter('entity_id', array('neq' => $product->getId()))
            ->addStoreFilter()
            ->setPageSize(self::PRODUCT_LIMIT)
            ->setCurPage(1);

        $this->_itemCollection->getSelect()->columns(
            array('price_distance' => new Zend_Db_Expr('ABS(price_index.final_price - ' . $product->getFinalPrice() . ')'))
        );

        $this->_itemCollection->getSelect()->order('price_distance');

        if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($this->_itemCollection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }

        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($this->_itemCollection);

        $this->_itemCollection->load();

        return $this;
    }
}
